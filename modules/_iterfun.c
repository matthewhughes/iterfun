#define PY_SSIZE_T_CLEAN
#include <Python.h>

static PyObject *
sum_bool(PyObject *self, PyObject *iterable)
{
    PyObject *it, *item;
    PyObject *(*iternext)(PyObject *);
    int cmp;
    unsigned count = 0;

    it = PyObject_GetIter(iterable);
    if (it == NULL)
        return NULL;
    iternext = *Py_TYPE(it)->tp_iternext;

    for (;;) {
        item = iternext(it);
        if (item == NULL)
            break;
        cmp = PyObject_IsTrue(item);
        Py_DECREF(item);
        if (cmp < 0) {
            Py_DECREF(it);
            return NULL;
        }
        if (cmp > 0)
            count++;
    }
    Py_DECREF(it);
    if (PyErr_Occurred()) {
        if (PyErr_ExceptionMatches(PyExc_StopIteration))
            PyErr_Clear();
        else
            return NULL;
    }
    return PyLong_FromLong(count);
}

static PyMethodDef IterFunMethods[] = {
    {"sum_bool", sum_bool, METH_O,
    "Count the number of entries in an iterable that evaluate to True"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef iterfun_module = {
    PyModuleDef_HEAD_INIT,
    "_iterfun",             /* Module name */
    "Fun with iterators",   /* Module documentation */
    -1,
    IterFunMethods
};

PyMODINIT_FUNC
PyInit__iterfun(void)
{
    return PyModule_Create(&iterfun_module);
}
