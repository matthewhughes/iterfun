import unittest

from iterfun import sum_bool

class TestCountBool(unittest.TestCase):
    def test_with_basic_bool(self):
        data = (True, True, False, True)
        self.assertEqual(sum_bool(data), 3)

    def test_with_mixed_types(self):
        data = ("", 0, False, 1, "hello", b"no", True)
        self.assertEqual(sum_bool(data), 4)

    def test_without_iterator(self):
        with self.assertRaises(TypeError):
            sum_bool(1)
