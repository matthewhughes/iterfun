import pytest

from iterfun import sum_bool


@pytest.mark.parametrize(
    ("iterator", "expected"),
    (
        ((False, 0, None), 0),
        ((True, "1"), 2),
    ),
)
def test_sum_bool(iterator, expected):
    assert sum_bool(iterator) == expected
