=======
iterfun
=======

Not very interesting, just a project for learning about C extensions to PYTHON.

sum_bool
========

Sum the number of entries in an interable that evaluate to true. Written in C,
equivalent to ``sum(map(bool, my_iter))``

.. code-block:: python

   >>> from iterfun import sum_bool
   >>> sum_bool(("", 0, False, 1, "hello", b"no", True))
   4
