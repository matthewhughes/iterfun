from setuptools import Extension, setup

setup(
    ext_modules=[
        Extension("_iterfun", ["modules/_iterfun.c"]),
    ],
)
